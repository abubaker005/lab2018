package abubaker;



import java.util.List;

public class ProfileBuilder 
{

	public static void createProfile(List profile)
	{
		JobSeeker seeker=new JobSeeker();
		for(int i=0;i<profile.size();i++)
		{
			seeker.addProfileElement(profile.get(i));
		}
	}
	
	public static void createSimpleProfile(JobSeeker s)
	{

		s.profile.add(Singleton.getInstance());
		s.profile.add(new Education());
		s.profile.add(new Experience());
		s.profile.add(new Skill());
	}
	

}
