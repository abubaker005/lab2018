package abubaker;
/**
 * 
 * @author Syed Abubaker
 *
 */
public class Experience {
	

		private String jobTitle;
		private String startDate;
		private String endDate;
		private String description;
		
		
		
		public Experience() {
			super();
			// TODO Auto-generated constructor stub
		}
		
		
		
		
		public Experience(String jobTitle, String startDate, String endDate, String description) {
			super();
			this.jobTitle = jobTitle;
			this.startDate = startDate;
			this.endDate = endDate;
			this.description = description;
		}




		public String getJobTitle() {
			return jobTitle;
		}
		public void setJobTitle(String jobTitle) {
			this.jobTitle = jobTitle;
		}
		public String getStartDate() {
			return startDate;
		}
		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}
		public String getEndDate() {
			return endDate;
		}
		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
	}



