package abubaker;
/**
 * 
 * @author Syed Abubaker
 *
 */
public interface JobApplier {
	public void applyForJob();

}
